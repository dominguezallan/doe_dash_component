
function initDate () {
  startDate = monthAgoString;
  endDate = todayString;
  $('input[name="daterange"]').val(formatToDDMMYYYY(startDate) + " - " + formatToDDMMYYYY(endDate));
  $('#over-date-range').text("From dates " + formateToLongDate(startDate) + " to " + formateToLongDate(endDate));
}

function formatToDDMMYYYY (date) {
  let dateElems = date.split('-');
  let temp = dateElems[0];
  dateElems[0] = dateElems[2];
  dateElems[2] = temp;
  return dateElems.join('/');
}

function formateToLongDate(date) {
  let momDate = moment(date);
  return momDate.format("Do MMM YYYY");
}

function changeDate() {
  // date range picker for Key Metrics (Page Views)
  $(function() {
    $('input[name="daterange"]').daterangepicker({
      maxDate: today,
      opens: 'left',
      "locale": {
        "format": "DD/MM/YYYY"
      }
    }, function(start, end, label) {
      // console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
      startDate = start.format('YYYY-MM-DD');
      endDate = end.format('YYYY-MM-DD');
      $(document).trigger(jQuery.Event("date-changed"));
    });
  });
}

function clearData() {
  // No need to clear visits, visitors, page views data
  viewsGraph.removeData();
  refDonut.removeData();
  newRetDonut.removeData();
  browserDonut.removeData();
  mobileDonut.removeData();
  osHbar.removeData();
  topHbar.removeData();
  $('#visits-current').animateNumber({
    number:0
  }, 500);
  $('#visitors-current').animateNumber({
    number:0
  }, 500);
  $('#pageviews-current').animateNumber({
    number:0
  }, 500);
  $('#new-perc').animateNumber({
    number:0
  }, 500);
}

$( document ).on("date-changed", function(event) {
  document.getElementById("start-load").style.display = "block";
  clearData();
  $('#over-date-range').text("From dates " + formateToLongDate(startDate) + " to " + formateToLongDate(endDate));
  sendRequests(startDate, endDate);
});
