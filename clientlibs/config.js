/* Configuration Variables
    confg.username: Your Web Services Username. This can be found in the Admin Console
    config.secret: Your Web Services Secret. This can also be found in the Admin Console
    config.reportSuite: The report suite you want to call the data from
    config.endpoint: The endpoint you will be hitting
*/

var config = {
    username:     "communications@det.nsw.edu.au:NSW department of education",
    secret:       "d787195c4cb6effbbbd0646336ff0b92",
    reportSuite:  "nsweducationprod",
    endpoint:     "api.omniture.com"
};
